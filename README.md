# tidal\_albums\_to\_playlist.py

Create a Tidal playlist from saved albums filtering by genres, artist, title, and/or years.  Genres are retrieved
from Discogs because Tidal does not reveal genres through its API and because Discogs
provides more accurate recording date information.

## Use

To create a local database of saved albums named myalbums.json:

    python tidal_albums_to_playlist.py --file myalbums.json

If --file is not specified, the default file name albumsdb.json is used.

It can take an hour or more to create this local database, so it is best to write it to a file
if more than one playlist is to be created.

To create a playlist in chronological order of saved blues albums that were
first released between 1930 and 1950, while using and updating if necessary the local album database albumsdb.json:

    python tidal_albums_to_playlist.py --genre blues --years 1930 1950

The same as before, but perform a dry run, and don't actually create the playlist, just show 
what it would contain:

    python tidal_albums_to_playlist.py --dryrun --genre blues --years 1930 1950

To create three playlists of tracks in chronological order from from saved jazz albums that are not also blues albums:

    python tidal_albums_to_playlist.py --genre jazz --except blues --split 3 

To create a shuffled playlist named My Favs of tracks from saved albums by Miles Davis or Dizzy Gillepsie:

    python tidal_albums_to_playlist.py --name 'My Favs' --artists miles dizzy --shuffle

If you create a playlist with the name of one of your existing playlists, the existing playlist is replaced.

To see all options, execute

    python tidal_albums_to_playlist.py --help

## Required Python packages

    pip install tidalapi python3_discogs_client argparse

The home of tidalapi is https://github.com/tamland/python-tidal.

The home of python3_discogs_client is https://github.com/joalla/discogs_client.

## Getting Discogs and Tidal credentials 

* On the Discogs web page, login and click on your icon at the top right, then select Settings, Developers, Generate New Token.  Set the shell variable DISCOGS_TOKEN to this value in your .profile or .bashrc. For example:

        export DISCOGS_TOKEN=ABC1234567890

Alternatively, you could edit the program to set DISCOGS\_TOKEN.

* When you launch the app, it will ask you to visit a URL in your default browser where
you will enter your Tidal login and password.

## Matching Tidal albums to Discogs albums

There is no universal code for record albums. The program matches Tidal albums to Discogs albums using the following heuristics in order:

1. The Tidal first artist name and album title are converted to ascii, because unicode characters do not always match correctly between the Tidal and Discogs.

2. The artist and title are matched against the program's internal rewrite database (see below) and modified if appropriate.  If the rewrite database also contains the year or genres, those values are used instead of 
checking Discogs.  The default rewrite database name is "rewritedb\_json".

3. Substrings in the title that are in parentheses or square brackets are removed from the title.

4. Discogs is searched for an album release using the artist and title search fields.

5. If that search fails, Discogs is searched for an album using a simple search of the artist concatenated with the title.


## Album years

The program attempts to determine the year the album was recorded rather than the years of later releases.  It uses the following heuristics in order:

1. If the year is specified in the program's internal rewrite database, that year is used.
2. If a year appears in the title of the album (that is, a 4-digit number beginning 19 or 20), that year is used.  Both the title from Tidal and the title from the corresponding Discog album are considered.
3. Otherwise, all the releases returned by the Discogs search API are examined, and the earliest year is used.

## Fixing lookup errors

Once in a while, a Tidal album cannot be found easily on Discogs because its name or the name of its artist differs between the two sites.  There are also problems when the earliest release date in Discogs is years later than the recording date, which is common for compilations.

You can edit the file rewrite\_db.json to manually specify how an album title and/or artist should be rewritten in order to be found in Discogs and to optionally specify a recording data.  The format of each record is

    [
        title pattern, 
        title replacement, 
        artist pattern, 
        artist replacement, 
        year recorded, 
        genre string
    ]

Only "title pattern" is required. If a pattern field is the empty string, it matches everything. If
a replacement field is the empty string, the title or artist is unchanged. If the year is included, it overrides the year listed in Discogs.  If the genre string is included, it overrides genre information listed in Discogs.  Including both year recorded and genre string means that Discogs is not checked at all for the album. Upper and lower case are not distinguished.

For example, this changes the album title "Wilbur Ware Super Bass" to "Super Bass".

    [
        "Wilbur Ware Super Bass",
        "Super Bass"
    ],

This specifies the recording year and genres of the album "Born Myron Carlton Bradshaw".

    [
        "Born Myron Carlton Bradshaw",
        "",
        "",
        "",
        1953,
        "#jazz#swing#"
    ]

This forces all albums by Frank Zappa to have the genres "rock" and "alternative".
 
    [
        "",
        "",
        "Zappa",
        "",
        0,
        "#rock#alternative#"
    ],