#!/bin/bash
python tidal_albums_to_playlist.py --genre jazz --except blues --years 1900 1939
python tidal_albums_to_playlist.py --genre jazz --except blues --years 1940 1949
python tidal_albums_to_playlist.py --genre jazz --except blues --years 1950 1956
python tidal_albums_to_playlist.py --genre jazz --except blues --years 1957 1959
python tidal_albums_to_playlist.py --genre jazz --except blues --years 1960 1961
python tidal_albums_to_playlist.py --genre jazz --except blues --years 1962 1969
python tidal_albums_to_playlist.py --genre jazz --except blues --years 1970 1979
python tidal_albums_to_playlist.py --genre jazz --except blues --years 1980 1999
python tidal_albums_to_playlist.py --genre jazz --except blues --years 2000 2023

