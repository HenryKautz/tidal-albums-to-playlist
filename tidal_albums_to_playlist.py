# Script that creates a tidal playlist based on saved albums filtered by genre
# Get genre information from Discogs since Tidal does not reveal genres



import discogs_client

import tidalapi
import random
import argparse  
import time
import string
import os
import re
import json
from math import ceil 
import traceback
import pickle

DISCOGS_TOKEN = str(os.environ.get('DISCOGS_TOKEN'))

REWRITE_DB = []
# Rewrite entry: (title pattern, title replacement, artist pattern, artist replacement, year recorded, genre)
# Only title pattern is required.
# Patterns match as substrings of original tile or artist.
# Capitalization is not significant.
# If both title pattern and artist pattern is specified, then both must match.
# Replacements that are missing or empty string are the original title or artist (no change).
# If both the year recorded and genre are specified, then a matching album is not
# looked up in Discogs.
    
NOT_A_NAME = ['jazz', 'orchestra', 'modern', 'contemporary', 
              'of', 'and', 'de', 'jug', 'band','original', 'various', 'artists', 'half',
              'nerf', 'ghost', 'man',
              'milk','of']
# Words that are not possibly a name, used when sorting by last name

def rewriter(artist:str, title:str)->tuple[str,str,int,str]:
    for (record) in REWRITE_DB:
        title_pattern = record[0]
        title_replacement = record[1] if len(record)>1 and record[1] else title
        artist_pattern = record[2] if len(record)>2 else ''
        artist_replacement = record[3] if len(record)>3 and record[3] else artist
        year = record[4] if len(record)>4 else 0
        genre = record[5] if len(record)>5 else ''

        if artist_pattern.casefold() in artist and title_pattern.casefold() in title:
            return (artist_replacement, title_replacement, year, genre)
    
    return (artist, title, 0, '')


def any_none_words(good:list[str], bad:list[str], s:str):
    # returns True iff any of the good strings and none of the bad strings
    # are substrings of s (or if exact are equal to s).  Matches have
    # to be on word boundaries or # symbols.
    okay = False
    if len(good)==0:
        okay = True
    else:
        for item in good:
            if re.search(r"\b"+item+r"\b", s, flags=re.I):
                okay = True
                break
    if not okay:
        return False
    for item in bad:
        if re.search(r"\b"+item+r"\b", s, flags=re.I):
           return False
    return True



# Search Discogs
# declare variables used by discosearch
d = None
total_requests = 0
last_time = time.time()
my_discogs_token = None
def discosearch(*positional, **keyword) -> list:
    global d, total_requests, last_time
    # Sleep for 2 seconds between Discogs searches, otherwise Discogs will throttle
    time.sleep(2)

    # Every 30 requests print the request rate
    total_requests += 1
    now = time.time()
    if total_requests % 30 == 0:
        delta = now - last_time
        last_time = now
        rate = round(30 / (delta/60))
        print(f"Discogs rate = {rate} requests per minute")
    
    # Try the request.  If Discogs clients is not running then create it.
    # If the request fails, then kill the Discogs client and return an empty list of results.  
    try:
        if not d:
            d = discogs_client.Client('tidal_albums_to_playlist', user_token=my_discogs_token)
            print("Created Discogs client")
            d.set_timeout(connect=15, read=15) # type:ignore
        return list(d.search(*positional,**keyword).page(0))
    except:
        print(f"Discogs failed searching for ", end='')
        if positional:
            print(f"{positional} ", end='')
        print(f"{keyword}")
        d = None
        return []


def authorize():
    session = tidalapi.Session()
    try:
        print('Trying to connect to Tidal using ~/.credentials/tidal.pickle')
        with open(os.path.expanduser('~/.credentials/tidal.pickle'), 'rb') as f:
            creds = pickle.load(f)
        session.load_oauth_session(creds['token_type'], creds['access_token'], 
                                   creds['refresh_token'], creds['expiry_time'])
    except:
        try:
            print('Creating new credentials for Tidal')
            session.login_oauth_simple() 
            creds = { 'token_type':session.token_type, 'access_token':session.access_token,
                    'refresh_token':session.refresh_token, 'expiry_time':session.expiry_time } 
            os.makedirs(os.path.expanduser('~/.credentials'), exist_ok=True)
            with open(os.path.expanduser('~/.credentials/tidal.pickle'), 'wb') as f:
                pickle.dump(creds,f)
            print('Credentials saved in ~/.credentials/tidal.pickle')
        except Exception as exception:
            print(traceback.format_exc()) 
            print(f"Tidal OAuth failed: {exception}")
            exit(1)
    return session   

def albums_to_playlist(
        discogs_token:str,
        playlist_name:str = '',
        genre:list[str] = [],
        notgenre:list[str] = [],
        artists:list[str] = [],
        titles=[],
        startyear:int = 0,
        endyear:int = 3000,
        in_datafile:str='',
        out_datafile:str='',
        split:int=1,
        shuffled:bool=False,
        new:bool=False,
        sortby='year',
        rewrite_datafile:str=''
        ):
    
    global my_discogs_token
    global REWRITE_DB

    if rewrite_datafile:
        try:
            with open(rewrite_datafile,'r') as f:
                REWRITE_DB = json.loads(f.read())
            print(f'Read rewrite database {rewrite_datafile}')
        except:
            print(f'Warning: could not open {rewrite_datafile}')


    my_discogs_token = discogs_token
          

    # Create Tidal client
    session = authorize()
    me = session.user
    tidal_id = me.id #type:ignore

    #
    # Set up album database
    #

    albums_db = []
    albums_in_db = set()
    new_albums = []
    if in_datafile:
        # Load albums database from file
        try:
            with open(in_datafile,'r') as f:
                albums_db = json.loads(f.read())
            for a in albums_db:
                albums_in_db.add(a['album_id'])
            print(f"Read album database {in_datafile}")
        except:
            print(f"Creating new album database {in_datafile}")
    
    if not albums_db or out_datafile or new:
        # Create or update albums database 
    
        # Get saved albums
        tidal_albums = []
        offset = 0
        done = False

        favorites = tidalapi.user.Favorites(session,tidal_id)
        
        tidal_albums += favorites.albums()
        
        print(f"Found {len(tidal_albums)} saved albums")

        # Create or expand database of albums combining data from Tidal and Discogs
        original_title = ''
        original_artist = ''
        for album in tidal_albums:
            album_id = album.id
            if album_id not in albums_in_db:
                try:
                    new_albums.append(album_id)
                    album_title = album.name
                    primary_artist_name = album.artists[0].name
                    original_title = album_title
                    original_artist = primary_artist_name

                    # Look up the album genres on Discogs
                    #   Covert title and artist to ascii
                    #   Remove material in parenthesis from title
                    #   Drop substrings in () or [] in title
                    #   Drop artists named "various artists"
                    #   Rewrite title and artist

                    
                    album_title = album_title.casefold()
                    primary_artist_name = primary_artist_name.casefold()


                    # If artist is something like "Various Artists" then ignore it
                    if re.search('artists', primary_artist_name, flags=re.I):
                        primary_artist_name = ''
                    
                    # Handle special cases using rewrite database

                    (primary_artist_name, album_title, year_recorded,genres_string) = rewriter(primary_artist_name,album_title)

                    discogs_id = ''
                    discogs_type = ''
                    discogs_url = ''

                    # Determine genres if not already set by rewriter
                    if genres_string == '' or year_recorded <=0:

                        # Eliminate material in ()'s or []'s from title
                        album_title = re.sub('\\(.*\\)', '', album_title).strip()
                        album_title = re.sub('\\[.*\\]', '', album_title).strip()

                        # Search Discogs
                        # results = discosearch(title=album_title, type='master', artist=primary_artist_name)
                        # discogs_type = 'master'
                        # if (len(results)==0):
                        #     results = discosearch(type='release', title=album_title, artist=primary_artist_name)
                        #     discogs_type = 'release'

                        results = discosearch(title=album_title, artist=primary_artist_name)
                        if len(results) == 0:
                            results = discosearch(primary_artist_name + ' ' + album_title)

                        # No hits in Discogs
                        if len(results) == 0:
                            print(f"Discogs did not find {primary_artist_name}, {album_title}")
                            print(f"  Skipping {original_artist}, {original_title}")
                            continue
                    
                        # Get the discogs id
                        discogs_id = results[0].id
                        # discogs_uri = results[0].uri # Current python library does not provide this even though API does
                        discogs_url = 'https://discogs.com/' + re.sub('/[^/]*/','',results[0].url)

                        if genres_string == '':
                            # Get all the genres. Discogs makes an arbitrary separation between genres and styles, so use both.
                            # print(f"working on {original_artist}, {original_title}")
                            album_genres=[]
                            for i in range(len(results)):
                                album_genres = getattr(results[i], 'genres', [])
                                if album_genres != None and len(album_genres)>0:
                                    album_styles = getattr(results[i], 'styles', [])
                                    if album_styles != None:
                                        album_genres += album_styles
                                    break
                            if len(album_genres) == 0:
                                print(f"Warning: No genres found for {original_artist}, {original_title} as {primary_artist_name}, {album_title}")
                            
                            genres_string = '#' + ('#'.join(album_genres)).casefold() + '#'

                        # Determine the year recorded if it was not already manually set by rewriter()
                        if year_recorded <= 0:
                            year_recorded = 9999

                            for i in range(min(200,len(results))): #type:ignore
                                if hasattr(results[i],'year'):
                                    year = int(results[i].year) #type:ignore
                                else:
                                    year = 0
                                if year > 0 and year < year_recorded:
                                    year_recorded = year

                            # Overide Discogs year if Spotify album name contains a year
                            # Uses original title because year might be in ()'s which was deleted for Discog search
                            match_obj = re.search("(19|20)[0-9][0-9]", original_title)
                            if match_obj:
                                year = int(match_obj.group(0))
                                if year < year_recorded:
                                    year_recorded = year
                            if year_recorded == 9999:
                                print(f"Warning, no year found for {original_artist}, {original_title}, using 9999")
                    # Add album to database
                    albums_db.append({
                        'album_title': original_title,
                        'album_artist': original_artist,
                        'album_year': year_recorded,
                        'album_id': album_id,
                        'discogs_type': discogs_type,
                        'discogs_id': discogs_id,
                        'discogs_url': discogs_url,
                        'track_ids': [ str(track.id) for track in album.tracks() ],
                        'genres_string': genres_string
                        })
                    # Show progress
                    
                    print()
                    print(f"+ {original_artist}, {original_title}, {year_recorded}, {genres_string}")
                    # print('+',end='',flush=True)
                # except AssertionError as exception:
                except Exception as exception:
                    print(traceback.format_exc()) 
                    print(f"Unexpected exception during Discogs processing: {exception}")
                    print(f"Unable to handle {original_artist}, {original_title}")
                    continue
            else:
                print('-',end='', flush=True)

        print()
    #
    # Sort the album database by date
    #
    albums_db.sort(key=lambda a: a['album_year'])
    #
    # Save the albums database to a file
    #
    if out_datafile:
        with open(out_datafile, 'w') as f:
            f.write(json.dumps(albums_db, indent=2))
            print(f"Wrote album database {out_datafile}")
    
    #
    # Filter albums by genre and year and save tracks
    #

    # Optional sorting of albums by specified key
    if sortby != 'year':
        if sortby == 'title':
            for item in albums_db:
                item['key'] = re.sub('^The ','',item['album_title'],flags=re.IGNORECASE)
        elif sortby == 'firstname':
            for item in albums_db:
                item['key'] = re.sub('^The ','',item['album_artist'],flags=re.IGNORECASE)
        else: # lastname
            for item in albums_db:
                key = re.sub('^The ','',item['album_artist'],flags=re.IGNORECASE)  
                key = re.sub(r'[^\w ]','',key)
                key = key.casefold()
                splitkey = key.split()
                if len(splitkey)>1 and  splitkey[0] not in NOT_A_NAME and  splitkey[1] not in NOT_A_NAME:
                    remainder = ' '.join(splitkey[2:])
                    key = splitkey[1] + ' ' + splitkey[0] + ((' ' + remainder) if remainder else '')
                item['key'] = key        
        albums_db.sort(key=lambda a: a['key'])
        with open(out_datafile, 'w') as f:
            f.write(json.dumps(albums_db, indent=2))
            print(f"Wrote album database {out_datafile}")

    all_tracks = []
    num_matching_albums = 0
    for a in albums_db:
        genres_string = a['genres_string']
        track_ids = a['track_ids']
        album_year = a['album_year']
        album_title = a['album_title']
        album_artist = str(a['album_artist'])
        album_id = a['album_id']
        # If album matches genre, year range, and artists, then add its tracks to playlist
        if ( any_none_words(genre, notgenre, genres_string) and
            startyear <= album_year and album_year <= endyear and
            any_none_words(artists, [], album_artist.casefold()) and
            any_none_words(titles, [], album_title.casefold()) and
            ((not new) or album_id in new_albums)
            ):
                num_matching_albums += 1
                print(f"Adding {len(track_ids)} tracks from {album_artist}, {album_title} ({album_year}): {genres_string}")
                for id in track_ids:
                    all_tracks.append((id,album_id))
    print(f"Number of albums is {num_matching_albums}")
    print(f"Total number of tracks is {len(all_tracks)}")
    #
    # Create Tidal playlist
    #
    if playlist_name:

        if shuffled:
            # Shuffle tracks
            random.shuffle(all_tracks)
        if split < 1:
            split = 1
        total_tracks = len(all_tracks)
        if total_tracks / split > 9900: # Add a little slop to allow for uneven split
            split = ceil(total_tracks/9900)
            print(f"Warning: Setting {split=} due to 10,000 track playlist limit")
        split_size = ceil(total_tracks/split)
        offset=0
        for playlist_number in range(1,split+1):
            if split == 1:
                playlist_split_name = playlist_name
            else:
                playlist_split_name = f"{playlist_name} {playlist_number:0{len(str(split))}d}"
            #
            # Create or update playlist
            #
            my_playlists = []
            print('Getting user playlists ... ')
            my_playlists = me.playlists() #type:ignore
            
            for playlist in my_playlists:
                if playlist.name == playlist_split_name:
                    playlist.delete()  #type:ignore
                    # sp.playlist_replace_items(playlist_id,[])
                    print(f"Deleting old playlist {playlist_split_name}")
                    
            time.sleep(2)
            
            playlist = me.create_playlist(playlist_split_name, "Created by tidal_albums_to_playlist") #type:ignore
            print(f"Creating playlist {playlist_split_name}")
            # Compute exact size of the current split, might not equal split_size
            remaining_this_split = min(split_size, total_tracks-offset)        
            #for unshuffled playlist, don't split an album
            if not shuffled:
                # Possibly enlarge the current split in order to finish off an album
                last_position = offset + remaining_this_split - 1
                last_album = all_tracks[last_position][1]
                while last_position + 1 < len(all_tracks) and last_album == all_tracks[last_position+1][1]:
                    last_position += 1
                remaining_this_split = last_position - offset + 1
            size_this_split = remaining_this_split
            # Add albums to playlist in chunks
            while remaining_this_split > 0:
                chunksize = min(100, remaining_this_split)
                chunk = [ id for (id,_) in all_tracks[offset:offset+chunksize] ]
                playlist.add(chunk)
                # sp.playlist_add_items(playlist_id=playlist_id, items=chunk)
                remaining_this_split -= chunksize
                offset += chunksize
                print(f".", flush=True, end='')
            print(f"\nAdded {size_this_split} tracks to playlist {playlist_split_name}")


def main():
    parser = argparse.ArgumentParser(description="Create playlist from saved albums filtered by genre, artists, title, and/or year")
    parser.add_argument('--dryrun', '-d', action='store_true', help='Do not save playlist to Tidal')
    parser.add_argument('--name', '-n', type=str, default='', help='Name of playlist, if omitted name is created from genre')
    parser.add_argument('--genre', '-g', action='extend', default=[], type=str, nargs='+', help='One or more genres to include')
    parser.add_argument('--except', '-x', dest='notgenre', metavar='GENRE', action='extend', default=[], type=str, nargs='+', help='One or more genres to exclude')
    parser.add_argument('--artists', '-a', action='extend', default=[], type=str, nargs='+', help='One or more artists to include')
    parser.add_argument('--title', '-t', action='extend', default=[], type=str, nargs='+', help='One or more (partial) titles to include')
    parser.add_argument('--years', '-y', type=int, metavar=('START_YEAR','END_YEAR'), default=[0,1000000], nargs=2, help='Only match albums between pair of years inclusive')
    parser.add_argument('--file', '-f', type=str, default='albumsdb.json', metavar='FILE', help='local database file')
    parser.add_argument('--split', '-p', type=int, default=1, help='Create N numbered playlists')
    parser.add_argument('--shuffle', '-s', action='store_true', help='Shuffle playlist') 
    parser.add_argument('--new', '-N', action='store_true', help='Only include albums not already in local database')
    parser.add_argument('--rewrite', '-R', type=str, default='rewrite_db.json', metavar='FILE', help='rewrite database')
    parser.add_argument('--sortby', '-S', choices=['year', 'title', 'firstname', 'lastname'], default='year', help='Sort albums in playlists')
    args = parser.parse_args()
    dryrun:str = args.dryrun
    genre:list[str] = args.genre
    notgenre:list[str] = args.notgenre
    titles:list[str] = args.title
    name:str = args.name 
    artists = args.artists
    genre = [ s.casefold() for s in genre ]
    notgenre = [ s.casefold() for s in notgenre]
    artists = [ s.casefold() for s in artists]
    titles = [ s.casefold() for s in titles]
    split = args.split if args.split >= 1 else 1


    if not name:
        if args.shuffle: 
            name = 'Shuffled'
        else:
            name = 'My'
        if titles:
            name += ' ' + string.capwords(' '.join(titles))
        if genre:
            name += ' ' + string.capwords(' '.join(genre))
        name += ' Albums'
        if artists:
            name += ' By ' + string.capwords(' '.join(artists))
        if args.years[0]>0:
            name += ' ' + str(args.years[0]) + '-' + str(args.years[1])
    if dryrun:
        name = ''

    
    albums_to_playlist(

        DISCOGS_TOKEN,
        playlist_name=name,
        genre=genre,
        notgenre=notgenre,
        artists=artists,
        titles=titles,
        startyear=args.years[0],
        endyear=args.years[1],
        in_datafile=args.file,
        out_datafile=args.file,
        split=split,
        shuffled=args.shuffle,
        new=args.new,
        sortby=args.sortby,
        rewrite_datafile=args.rewrite
        )

if __name__ == '__main__':
    main()

